import "./styles/app.scss";
var $ = window.$||window.jQuery;

$(document).ready(function(){
  $(".nav-item, .down-arrow").click(function(e){
    e.preventDefault();

    var id     = $(this).attr("href");
    var offset = $(id).offset();

    $("html, body").animate({
      scrollTop: offset.top
    }, 600);
  });

});